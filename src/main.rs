extern crate csv;

use std::error::Error;
use std::process;
use std::env;

use csv::Reader;

#[derive(Clone, Debug)]
struct Airport {
    name: String,
    iata: String,
    lat: f32,
    lon: f32,
    distance: f32,
}

fn get_airports() -> Result<Vec<Airport>, Box<Error>> {
    let current_dir = env::current_dir().unwrap();
    let mut file_to_read: String = "".to_owned();

    file_to_read.push_str(current_dir.to_str().unwrap());
    file_to_read.push_str("/assets/world-airports.csv");

    let mut rdr = Reader::from_path(file_to_read)?;

    let mut airports: Vec<Airport> = Vec::new();

    for result in rdr.records() {
        let record = result.expect("a CSV record");
        let airport_type = record.get(2).unwrap();

        if airport_type == "large_airport" {
            let name =  String::from(record.get(3).unwrap());
            let lat =  String::from(record.get(4).unwrap()).parse::<f32>().unwrap();
            let lon =  String::from(record.get(5).unwrap()).parse::<f32>().unwrap();
            let iata =  String::from(record.get(13).unwrap());

            let airport: Airport = Airport { name, iata, lat, lon, distance: 0.0 };

            airports.push(airport);
        }
    }
    Ok(airports)
}

fn find_matching_airports(airports_list: Vec<Airport>, iata_departure_airport: &str) -> Vec<Airport> {
    let mut matching_airports: Vec<Airport> = Vec::new();
    let departure_airport = airports_list.clone().iter().find(|airport| airport.iata == iata_departure_airport).unwrap().clone();

    let departure_lat = departure_airport.lat.to_radians();
    let departure_lon = departure_airport.lon.to_radians();

    for mut airport in airports_list {
        if airport.iata != iata_departure_airport && airport.iata.len() > 0 {
            let lat = airport.lat.to_radians();
            let lon = airport.lon.to_radians();

            let distance = (departure_lat.sin() * lat.sin() + departure_lat.cos() * lat.cos() * (departure_lon - lon).cos()).acos() * 6371.0;

            if 9000.0 <= distance && distance < 9800.0 {
                airport.distance = distance;

                matching_airports.push(airport);
            }
        }
    }

    return matching_airports;
}

fn sorting_matching_airports(airports_list: Vec<Airport>) -> Vec<String> {
    let mut ordered_list = Vec::new();
    let best_airports = vec!["ATL", "PEK", "DXB", "HND", "LAX", "ORD", "LHR", "HKG", "PVG", "CDG", "AMS", "DFW", "CAN", "FRA", "IST", "DEL", "CGK", "SIN", "ICN", "DEN", "BKK", "JFK", "KUL", "SFO", "MAD", "CTU", "LAS", "BCN", "BOM", "YYZ", "SEA", "CLT", "LGW", "SZX", "TPE", "MEX", "KMG", "MUC", "MCO", "MIA"];

    for airport in airports_list {
        let mut aiport_name: String = "".to_owned();

        aiport_name.push_str(airport.iata.as_str());
        aiport_name.push_str(" - ");
        aiport_name.push_str(airport.name.as_str());
        aiport_name.push_str(" - ");
        aiport_name.push_str(&airport.distance.to_string());
        aiport_name.push_str("km");

        if best_airports.contains(&airport.iata.as_str()) {
            aiport_name.insert_str(0, "*** ");
        }

        ordered_list.push(aiport_name);
    }

    ordered_list.sort_by(|a, b| a.cmp(b));

    return ordered_list;
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Usage: ./prog <IATA_CODE_AIRPORT>");
        process::exit(1);
    }

    let iata_departure_airport = &args[1];

    println!("Loading airports...");
    let airports = get_airports();

    if let Err(err) = airports {
        println!("error running example: {}", err);
        process::exit(2);
    }

    println!("Airports are loaded");
    let airports_list: Vec<Airport> = get_airports().unwrap();

    println!("Finding matching airports...");
    let matching_airports = find_matching_airports(airports_list, iata_departure_airport);
    println!("Matching airports found");

    println!("Sorting matching airports...");

    let sorted_airports = sorting_matching_airports(matching_airports);

    for airport in sorted_airports {
        println!("{:?}", airport);
    }

}
